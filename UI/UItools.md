# All the tools for modern UIs

# WEB
Tailwind
MaterialUI
Bootstrap

## Icons
https://tablericons.com/
1424 Open source free SVG icons. Highly customizable. No attribution required. For commercial use.

https://heroicons.com/
Beautiful hand-crafted SVG icons, by the makers of Tailwind CSS

https://react-icons.github.io/react-icons
MISC svgs


## Backgrounds
https://www.svgbackgrounds.com/

https://hypercolor.dev/
A curated collection of beautiful Tailwind CSS gradients using the full range of Tailwind CSS colors. Easily copy and paste the class names, CSS or even save the gradients as an image.

# COMMAND LINE

Dialog etc...
