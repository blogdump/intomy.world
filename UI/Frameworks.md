# FRAMEWORKS 2022+

## Small Project or components:
https://svelte.dev/


## Full frameworks

SvelteKIT
https://kit.svelte.dev/

AstroJS
https://astro.build/


## TOOLS:
https://vitejs.dev/
Next Generation Frontend Tooling

https://vite-pwa-org.netlify.app/
PWA integrations for Vite and the ecosystem
Zero-config and framework-agnostic
PWA Plugin for Vite


## AVOID: generally not required unless project is HUDE and with a big team.
Angular, React, Next.JS